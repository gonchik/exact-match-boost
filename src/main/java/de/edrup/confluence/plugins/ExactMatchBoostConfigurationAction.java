package de.edrup.confluence.plugins;

import javax.servlet.http.HttpServletRequest;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.opensymphony.webwork.ServletActionContext;

public class ExactMatchBoostConfigurationAction extends ConfluenceActionSupport {
	
	private static final long serialVersionUID = 1L;
	
	private final ExactMatchBoostHelper exactMatchBoostHelper;
	
	
	public ExactMatchBoostConfigurationAction(ExactMatchBoostHelper exactMatchBoostHelper) {
        this.exactMatchBoostHelper = exactMatchBoostHelper;
	}
	
	
	@Override
	public String execute() throws Exception {
		
		super.execute();
        
		HttpServletRequest request = ServletActionContext.getRequest();
        
        // in case the request contains our first input field we assume that we have a POST call here
        if(request.getParameter("boostFactor") != null) {
        	ExactMatchBoostSettings newSettings = new ExactMatchBoostSettings();
        	newSettings.boostFactor = Float.parseFloat(request.getParameter("boostFactor"));
        	newSettings.boostIncrease = Float.parseFloat(request.getParameter("boostIncrease"));
        	newSettings.scanAttachments = (request.getParameter("scanAttachments") != null) ? true : false; 
        	exactMatchBoostHelper.setSettings(newSettings);
        }
        
        return SUCCESS;
    }
	
	
	public ExactMatchBoostSettings getSettings() {
		return (exactMatchBoostHelper.getSettings() != null) ? exactMatchBoostHelper.getSettings() : new ExactMatchBoostSettings();
	}
}
