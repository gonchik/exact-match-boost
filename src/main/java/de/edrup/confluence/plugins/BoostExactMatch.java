package de.edrup.confluence.plugins;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexableField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.search.service.SearchQueryParameters;
import com.atlassian.confluence.search.v2.lucene.boosting.BoostingStrategy;
import com.atlassian.core.filters.ServletContextThreadLocal;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.opensymphony.webwork.ServletActionContext;


@Scanned
public class BoostExactMatch implements BoostingStrategy {
	
	private final ExactMatchBoostHelper exactMatchBoostHelper;
	
	private static final Logger log = LoggerFactory.getLogger(BoostExactMatch.class);

	private ExactMatchBoostSettings exactMatchBoostSettings;
	private long settingsLastChecked = 0L;
	private Set<String> toLoad = new HashSet<>(Arrays.asList("title", "type", "contentBody"));
	
	
	// constructor
	public BoostExactMatch(ExactMatchBoostHelper exactMatchBoostHelper) {
		this.exactMatchBoostHelper = exactMatchBoostHelper;
	}
	
	
	@Override
	public float boost(IndexReader reader, int doc, float score) throws IOException {
		
		try {

			// get the request
			HttpServletRequest request = ServletActionContext.getRequest();
			if(request == null) {
				request = ServletContextThreadLocal.getRequest();
			}
			
			if(request != null) {
				
				// check settings
				updateSettings();
				
				// get the query parameter
				String query = request.getParameter("queryString");
				if(query == null) {
					query = (request.getParameter("cql") != null) ? request.getParameter("cql") : ""; 
					query = query.contains("siteSearch") ? query.replaceFirst("siteSearch *~ *\"", "").replaceFirst("(?<!\\\\)\".*", "") : "";
				}
				
				if(query.length() > 0) {
										
					// get title and body
					// remark: according to Atlassian you shouldn't do the below and go via a FieldCache - however the contentBody does not make its way into the field cache and therefore we can't follow this rule
					Document document = reader.document(doc, toLoad);
					String title = document.getField("title").stringValue();
					boolean isAttachment = document.getField("type").stringValue().equals("attachment");
					IndexableField contentField = (!isAttachment || (isAttachment && exactMatchBoostSettings.scanAttachments)) ? document.getField("contentBody") : null;
					String body = (contentField != null) ? contentField.stringValue() : "";
					
					// get the boost factor
					Float boost = boostOnExactMatch(query, title, body);
					
					// do some logging
					if(log.isDebugEnabled()) {
						log.debug("Query: {}; title: {}; initial score: {}; boost: {}", query, title, score, boost);
					}
					
					// return boosted score
					return boost * score;
				}
			}
			
			// return score unmodified in case we were not able to do our job
			return score;
		}
		
		// also return the score unmodified in case of an exception
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
			return score;
		}
	}

	
	@Override
	public float boost(IndexReader reader, Map<String, Object> params, int doc, float score) throws IOException {
		return boost(reader, doc, score);
	}

	
	@Override
	public float boost(IndexReader reader, SearchQueryParameters query, int doc, float score) throws IOException {
		// remark: query is always null and can't be used for our purposes
		return boost(reader, doc, score);
	}
	
	
	// return boost factor in case an exact match was found
	// remark: this function assumes that Confluence already rated the results in terms of title vs content, how many matches etc.
	private float boostOnExactMatch(String query, String title, String body) {
		
		String items[] = query.replaceAll("[()~\\*\\\\]|( AND )|( NOT )|( OR )|( -)", " ").trim().toLowerCase().split("  *(?=([^\\\"']*[\\\"'][^\\\"']*[\\\"'])*[^\\\"']*$)");
		String compareAgainst = title.concat(body).toLowerCase();
		int itemsHit = 0;
		
		for(int n = 0; n < items.length; n++) {
			String itemToCheck = items[n].replace("\"", "").replace("'", "").trim();
			if(compareAgainst.contains(itemToCheck) && !itemToCheck.isEmpty()) {
				itemsHit++;
			}
		}
		return (itemsHit == 0) ? 1.0f : exactMatchBoostSettings.boostFactor + (itemsHit - 1) * exactMatchBoostSettings.boostIncrease;
	}
	
	
	// get the settings (only on a new request)
	private void updateSettings() {
		long now = new Date().getTime();
		if((now - settingsLastChecked) > 20000) {
			log.debug("Updating settings");
			settingsLastChecked = now;
			exactMatchBoostSettings = exactMatchBoostHelper.getSettingsRespectingLicense();
		}
	}	
	
	
	// get the detailed stack trace
	private static String getStackTrace(final Throwable throwable) {
	     final StringWriter sw = new StringWriter();
	     final PrintWriter pw = new PrintWriter(sw, true);
	     throwable.printStackTrace(pw);
	     return sw.getBuffer().toString();
	}
}
